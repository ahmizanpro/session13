<?php
//GLOBALS Variable 

$x = 100;   // superGlobal Variable means Associative array.

function doSomeThing()
{
    $x = 30; //this is local variable for this function
    echo $x + $GLOBALS["x"];  //superGlobal variable
}

doSomeThing();

echo "<pre>";
var_dump($GLOBALS);
echo "</pre>";

?>

<?php
//$_SERVER Variable

echo "<pre>";
var_dump($_SERVER);
echo "</pre>";

?>

<?php
//$_GET, $_POST, $_REQUEST variable

/***
 * GET method is not secured, Data shown into the URL link and "&" using between the  variables and input data
 * POST method is secured, by using this method everyone can pass the data in hidden and secured mood (Ex, ""form's"" data etc).
 * REQUEST method  to pick the data both of the POST and GET mothod.  
 */

echo "<pre>";
var_dump($_REQUEST);
echo "<pre>";

?>